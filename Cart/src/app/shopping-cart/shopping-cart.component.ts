import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { order, product } from '../shared/model';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  @Input('productAdded') products!: product[];
  showOrder=false;
  formGroup: FormGroup;
  order!:order;

  constructor(private productService: ProductService, private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      name: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  get name() { return this.formGroup.get('name'); }

  get phone() { return this.formGroup.get('phone'); }

  get address() { return this.formGroup.get('address'); }

  ngOnInit(): void {
    console.log(JSON.stringify(this.products));
  }

  updateQuantity(id: any, quantity: any) {
    var productExisting = this.products.find(x => x.id == id);

    if (productExisting) {
      productExisting.quantity = Number(quantity);
    }
  }

  sumOfPrice(): Number {
    var sum = 0;
    for (const iterator of this.products) {
      sum += iterator.price * iterator.quantity * ((100 - iterator.promotionPrice) / 100);
    }

    return sum;
  }
  deleteProduct(item: any) {
    const index = this.products.indexOf(item);
    if (index > -1) {
      this.products.splice(index, 1);
    }
  }


  addOrder() {
   
     this.order = {
      userName:this.formGroup.get('name')?.value,
      phoneNumber: this.formGroup.get('phone')?.value,
      address: this.formGroup.get('address')?.value,
      products: this.products
  
    }

    console.log(JSON.stringify(this.order));


    this.productService.addOrder(this.order).subscribe(res => {
      console.log(res);
      if (res.status == 200) {
        alert('Add successfully');
        this.showOrder = true;
      }
      else {
        alert('Failed!');
      }
    })
  }

}
