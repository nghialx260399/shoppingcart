
export interface product {
    id: string
    productName: string;
    quantity: number;
    price: number;
    promotionPrice: number;
    image: string
  }

  export interface order{
    userName:string;
    phoneNumber:string;
    address:string;
    products:product[];
  }

