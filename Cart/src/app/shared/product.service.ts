import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { product } from './model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient:HttpClient) { }

  getProducts(): Observable<HttpResponse<product[]>> {
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  addOrder(formData: any) {
    const data = JSON.stringify(formData);
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT/CheckOut';
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.post(apiUrl, data, options);
  }
}
