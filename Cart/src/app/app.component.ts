import { Component, OnInit } from '@angular/core';
import { product } from './shared/model';
import { ProductService } from './shared/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  products:any;
  productAdded:product[]=[];
  showCart=false;
  constructor(private productService:ProductService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(){
    this.productService.getProducts().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.products=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  addToCart(product:product){
    var obj={
      id:product.id,
      productName:product.productName,
      price:product.price,
      promotionPrice:product.promotionPrice,
      quantity:1,
      image:product.image
    };
    if(this.productAdded.length == 0)
    {
      this.productAdded.push(obj as product);
      alert('Đã thêm '+ product.productName+' vào giỏ hàng');
    }
    else
    {
      var existing = this.productAdded.find(x=>x.id == product.id);
      if(existing){
        existing.quantity += 1;
        alert('Đã thêm '+ product.productName+' vào giỏ hàng');
      }else{
        this.productAdded.push(obj as product);
        alert('Đã thêm '+ product.productName+' vào giỏ hàng');
      }
    }
    console.log(JSON.stringify(this.productAdded));
  }
}
